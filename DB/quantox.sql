-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2021 at 08:10 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quantox`
--

-- --------------------------------------------------------

--
-- Table structure for table `frameworks`
--

CREATE TABLE `frameworks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `frameworks`
--

INSERT INTO `frameworks` (`id`, `name`) VALUES
(1, 'Angular JS'),
(2, 'Angular 2'),
(3, 'React native'),
(4, 'Symfony'),
(5, 'Laravel'),
(6, 'Express\r\n'),
(7, 'NestJS');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`) VALUES
(1, 'Angular'),
(2, 'React'),
(3, 'Vue'),
(4, 'Php'),
(5, 'Node JS');

-- --------------------------------------------------------

--
-- Table structure for table `microframeworks`
--

CREATE TABLE `microframeworks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `microframeworks`
--

INSERT INTO `microframeworks` (`id`, `name`) VALUES
(1, 'Silex'),
(2, 'Lumen');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`) VALUES
(1, 'Front End Developer'),
(2, 'Back End Developer');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `position_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `framework_id` int(10) UNSIGNED DEFAULT NULL,
  `microframework_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `position_id`, `language_id`, `framework_id`, `microframework_id`) VALUES
(1, 2, 4, 4, 1),
(2, 2, 4, 5, 2),
(3, 2, 5, 6, NULL),
(4, 1, 1, 1, NULL),
(5, 1, 1, 2, NULL),
(6, 1, 2, 3, NULL),
(7, 1, 3, NULL, NULL),
(8, 2, 4, 4, NULL),
(9, 2, 4, NULL, NULL),
(10, 2, 4, 5, NULL),
(11, 2, 5, 7, NULL),
(12, 2, 5, NULL, NULL),
(13, 1, 1, NULL, NULL),
(14, 1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(200) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `roles_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `roles_id`) VALUES
(1, 'elena.nedeljkovik@hotmail.com', 'Elena Risteska', '$2y$10$cjJdDAXn8kZ7k5VB68kQquRTyE88AJlaHGjqNozaUsa86lVGVR8CS', 1),
(3, 'elena.elena@hotmail.com', 'Elena bla', '$2y$10$HKaMTaqaRIg2KUpAtulOT.DV.d7hpzYVEn6tS8adfefZq9NCxIANq', 1),
(4, 'alen@gmail.com', 'alen', '$2y$10$K8koHyutF6kD9XMkqBgRSedeEcZM0VywFCCsk.khUCddk0xSgjKES', 9),
(5, 'jovana@gmail.com', 'jovana', '$2y$10$8vrVs/IllqfR2jkGvlfPwuC47sOPm4uZSTNtIol.y1xz8PmzrOCYu', 2),
(6, 'elena@hot.com', 'elena', '$2y$10$4nieoBDEpKNi0/Jw7zN32efju7IVCpXO/GQNsFA8um823ZIqKpUhu', 2),
(7, 'goko_risteski@live.com', 'goko', '$2y$10$i000N6u3xdEZR05By8MntOWs9mAHUPTrFCbAw1/xzMiaI5HWXZzCi', 8),
(9, 'elenka@gmail.com', 'Elena Risteska', '$2y$10$gL83UEtQtNFYRVCpkjEN3e3wQrhbeC2dzOpaQPxOAbAcz77rRZAW.', 8),
(10, 'doe@doe.com', 'doe', '$2y$10$I15mKZ9.UDwMDsOL7UetNed/nrmAAv/BRPhu6X1jRUzzWbwLcelGK', 7),
(12, 'elena.nedeljkovik123@hotmail.com', 'Elenaa', '$2y$10$cLU8sKjfIr/BzSzIJjxcyO48HG/xhpkPt36N6beAJr1SdIBTpt.6C', 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `frameworks`
--
ALTER TABLE `frameworks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `microframeworks`
--
ALTER TABLE `microframeworks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `position_id` (`position_id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `microframework_id` (`microframework_id`),
  ADD KEY `framework_id` (`framework_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `roles_id` (`roles_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `frameworks`
--
ALTER TABLE `frameworks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `microframeworks`
--
ALTER TABLE `microframeworks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `framework_id` FOREIGN KEY (`framework_id`) REFERENCES `frameworks` (`id`),
  ADD CONSTRAINT `language_id` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `microframework_id` FOREIGN KEY (`microframework_id`) REFERENCES `microframeworks` (`id`),
  ADD CONSTRAINT `position_id` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `roles_id` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
