<?php require 'partials/header.php' ?>
<div class="container">
    <h2 class="text-center my-4">Login</h2>
    <div class="row">
        <div class="col-4 offset-4 mt-4">
            <?php


            if (isset($_SESSION['user'])) {
            header('Location:results');
            }

            if (isset($_SESSION['loginErr'])) {
                echo "<div class='alert alert-danger'>
                    {$_SESSION['loginErr']}
                </div>";
                unset($_SESSION['loginErr']);
            }
            ?>

            <form action="login" method="POST">
                <div class="form-group my-3">
                    <label for="email">Email:</label>
                    <input class="form-control" name="email" type="email">
                </div>
                <div class="form-group my-3">
                    <label for="password">Password:</label>
                    <input class="form-control" name="password" type="password">
                </div>

                <div class="d-flex justify-content-between">
                    <button class="btn btn-primary" type="submit"> Submit</button>
                    <a class="btn btn-warning" href="home">Back</a>
                </div>
            </form>

        </div>
    </div>
</div>





<?php require 'partials/footer.php' ?>