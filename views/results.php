<?php require_once 'partials/header.php';
use Core\App;
if(isset($_SESSION['user'])){
    $user = $_SESSION['user']->name;
}
if(isset($_SESSION['result'])){
    $result = $_SESSION['result'];
}
echo"<pre>";
$allIDS=App::get('controller')->printTree();
echo"</pre>";


?>
<div class="container">
            <?php if(!isset($_SESSION['user'])){ ?>
                <h2 class="text-center display-3">Please login first.</h2> 
            <?php }?>
            <?php 
                if(isset($_SESSION['user'])){ ?>
                    <h2 class="text-center display-3">Welcome    
                    <?php   $user = $_SESSION['user']->name; ?>
                    <span class="text-success text-capitalize display-3"> <?= $user?> </span> </h2>
                <?php } ?> 
                    
           
     
<div class="row">
<?php if(!empty($_SESSION['result']) && isset($_SESSION['user'])){?>
    <div class="col-8 offset-2">
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Language</th>
                    <th scope="col">Framework</th>
                    <th scope="col">Microframework</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach($result  as $id => $user){ ?>
                        <tr>
                            <th  scope="row"><?= $id+1?></th>
                            <td><?=$user->name?></td>
                            <td><?=$user->email?></td>
                            <td><?=$user->language?></td>
                            <td><?=$user->framework?></td>
                            <td><?=$user->microframework?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
<?php } ?>



        <div class="d-flex justify-content-center my-4">
            <a class="btn btn-warning m-2" href="home">Back</a>

           <?php if(isset($_SESSION['user'])){ ?>   
            <form action="logout" method="POST">
                <button class="btn btn-danger m-2" type="submit">Logout</button>
            </form>
            <?php } ?>
        </div>  
        
    </div>
</div>


</div>


<?php require_once 'partials/footer.php'?>