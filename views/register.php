<?php require 'partials/header.php';

use Core\App;
$allpositions = App::get('query')->selectPositions('roles');

?>
<div class="container">

    <h2 class="text-center my-4">Register</h2>
    <div class="row">
        <div class="col-4 offset-4 mt-4">
            <?php
            if (isset($_SESSION['user'])) {
                header('Location:results');
            }
        
            if (isset($_SESSION['validationErr'])) {
                $errors = $_SESSION['validationErr'];
                foreach ($errors as  $error) {
                    echo "<div class='alert alert-danger'>
                        {$error}
                    </div>";
                }
                unset($_SESSION['validationErr']);
            }
            ?>

            <form action="register" method="POST">
                <div class="form-group my-3">
                    <label for="roles_id">Please choose one of these types:</label>
                    <select class="form-control" name="roles_id">
                        <?php
                        foreach ($allpositions as $key => $position) {
                            echo "<option value='{$key}'>{$position}</option>";
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group my-3">
                    <label for="email">Email:</label>
                    <input  class="form-control" name="email" type="email">
                </div>
                <div class="form-group my-3">
                    <label for="name">Name:</label>
                    <input  class="form-control" name="name" type="text">
                </div>
                <div class="form-group my-3">
                    <label for="password">Password:</label>
                    <input   class="form-control" name="password" type="password">
                </div>
                <div class="form-group my-3">
                    <label for="Repetedpassword">Repeat password:</label>
                    <input  class="form-control" name="repetedPassword" type="password">
                </div>
                <div class="d-flex justify-content-between">
                    <button class="btn btn-primary" type="submit"> Submit</button>
                    <a class="btn btn-warning" href="home">Back</a>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require 'partials/footer.php' ?>