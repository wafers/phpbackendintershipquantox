<?php require 'partials/header.php';

use Core\App;
$allpositions = App::get('query')->selectAll('positions');
?>
<div class="container mx-auto">
    <div class="row">
        <div class="col-6 my-5 mx-auto">
        
            <h2 class="text-center m-4 display-4">Welcome</h2>
            <div class="d-flex justify-content-center">
                <a class="btn btn-primary mx-2" href="login">Login</a>
                <a class="btn btn-primary mx-2" href="register">Register</a>
            </div>
     
            <?php
           
    
                if (isset($_SESSION['homeERR'])) {
                    echo "<div class='alert alert-danger my-2'>
                            {$_SESSION['homeERR']}
                        </div>";
                    unset($_SESSION['homeERR']);
                }
            ?>
            <form class="mt-4" action="search" method="POST">
                <div  class="form-group my-3 ">
                    <label  for="searchInput">Search :</label>
                    <input  class="form-control" name="searchInput" id="searchInput" type="text">
                </div>
                <div  class="form-group mb-3">
                    <label for="userType">Choose user type:</label>
                    <select  class="form-control" name="userType" id="userType">
                        <?php
                        foreach ($allpositions as  $position) {
                            echo ' <option value="' . $position['id'] . '">' . $position['name'] . '</option>';
                        }
                        ?>
                    </select>
                </div>   
                <div class="d-flex justify-content-center">
                    <input class="btn btn-primary m-4" value="submit" type="submit">
                </div>
            </form>
        </div>
    </div>
</div>


<?php require 'partials/footer.php'; ?>