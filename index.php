<?php
require_once 'core/Router.php';
require_once 'core/database/Conection.php';
require_once 'core/App.php';
require_once 'core/database/Mysql.php';
require_once 'core/database/Query.php';
require_once 'core/controlers/HomeController.php';


use Core\App;
use Core\Router;
use Core\Database\Mysql;
use Core\Database\Query;
use Core\Controlers\HomeController;

session_start();

$pdo=Mysql::connect('127.0.0.1','quantox','root','');
$query= new Query($pdo);
App::bind('query',$query);
$controller= new HomeController($pdo);
App::bind('controller',$controller);
Router::handle();