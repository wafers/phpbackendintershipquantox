<?php
namespace Core;

use Exception;

class App
{
    protected static $services= [];
  
    public static function bind($key,$value)
    {
        self::$services[$key] = $value;
    }
    public static function get($key)
    {
       if(!array_key_exists($key, self::$services)){
            throw new Exception("This key -> {$key} is not binded to any class ");
        }
        return self::$services[$key];
    }

    
}