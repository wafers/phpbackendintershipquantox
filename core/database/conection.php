<?php
namespace Core\Database;

interface Conection
{
    public static function connect($host,$db,$username,$password): \PDO;
}
