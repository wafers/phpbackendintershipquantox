<?php
namespace Core\Database;
use PDO;

class Query 
{
  
    protected $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo= $pdo;
    }

    public function insert(string $table, array $users)
    {
        $sql = sprintf(
            'INSERT INTO %s(%s) VALUES(%s)',
            $table,
            implode(', ', array_keys($users)),
            ':' . implode(', :', array_keys($users))
        );
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($users);  
    }

    public function selectAll($table){
        $sql= sprintf( 'select * from `%s`', $table);
        $stmt=$this->pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function selectPositions($table){
        $data=[];
        $sql= sprintf(
            'SELECT roles.id, positions.name as position_name, 
            languages.name as languages_name,
            frameworks.name as frameworks_name,
            microframeworks.name as microframeworks_name
            FROM `%s`
            left JOIN languages on languages.id = roles.language_id 
            left join frameworks on frameworks.id = roles.framework_id 
            left join microframeworks on microframeworks.id = roles.microframework_id 
            left join positions on positions.id = roles.position_id
            ', $table 
            );

        $stmt=$this->pdo->prepare($sql);
        $stmt->execute();

        $queryData= $stmt->fetchAll(PDO::FETCH_OBJ);  
        foreach($queryData as $data1){
        
            if(!in_array($data1->languages_name,$data)&& $data1->languages_name!=null){
                $key= $data1->id;
                $data[$key]=$data1->languages_name;
            }
            if(!in_array($data1->frameworks_name,$data)&& $data1->frameworks_name!=null){
                $key= $data1->id;
                $data[$key]=$data1->frameworks_name;
            }
            if(!in_array($data1->microframeworks_name,$data)&& $data1->microframeworks_name!=null){
                $key= $data1->id;
                $data[$key]=$data1->microframeworks_name;
            }
        }
        return $data;
    }
}

