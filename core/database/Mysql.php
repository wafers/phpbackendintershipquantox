<?php

namespace Core\Database;

use PDO;
use PDOException;

class Mysql implements Conection
{
    public static function connect($host, $db, $username, $password): PDO
    {
        try{
            return new PDO("mysql:host={$host};dbname={$db}",$username,$password);
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }
}