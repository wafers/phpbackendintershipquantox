<?php
namespace Core;

class Router
{
    public static function handle()
    {
        $path= self::getPath();  
        self::getContent($path);
    }

    public  static function getPath()
    {
        $path= parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        return $path === '/'? '/home' : $path;
    }
    
    public static function getContent(string $path)
    {      
        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            require 'actions' . $path . '.php';
        }else{
            require 'views'. $path .'.php'; 
        }
    }
} 