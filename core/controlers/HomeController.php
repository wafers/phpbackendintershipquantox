<?php

namespace  Core\Controlers;

use PDO;
use Core\Database\Query;

class HomeController extends Query
{
    public function validate(array $request)
    {

        $errors = [];

        if ($_SERVER["REQUEST_METHOD"] == "POST") {


            if (!isset($request['roles_id'])) {
                $errors[] = "Please select a type";
            }

            if (empty($request['email']) && filter_var($request['email'], FILTER_VALIDATE_EMAIL)) {
                $errors[] = "Please insert valid e-mail";
            }

            if (empty($request['name'])) {
                $errors[] = "Please insert your name .";
            }

            if (empty($request['password'])) {
                $errors[] = "Please enter your password";
            }

            if (empty($request['repetedPassword']) || ($request['password'] !== $request['repetedPassword'])) {
                $errors[] = "Your password is not correct. Please repeat your password.";
            }
        }

        if (empty($errors)) {
            return true;
        } else {
            $_SESSION['validationErr'] = $errors;
            header('Location:register');
            return false;
        }
    }


    public function register(array $request)
    {
        if ($this->validate($request)) {
            unset($request['repetedPassword']);
            $request['password'] = password_hash($request['password'], PASSWORD_BCRYPT);
            $this->insert('users', $request);
        } else {
            header('Location:register');
        }
    }

    public  function login(array $request)
    {


        if (isset($request['email']) && !empty($request['email']) && isset($request['password']) && !empty($request['password'])) {
            $sql = "SELECT email, name , password  FROM users WHERE email ='{$request['email']}'";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            if (password_verify($request['password'], $user->password)) {
                session_start();
                $_SESSION['user'] = $user;
                header('Location:results');
            } else {
                $_SESSION['userNotExist'] = 'User does not exist.';
                header('Location:login');
            }
        } else {
            $_SESSION['loginErr'] = 'Email and password can not be empty.';
            header('Location:login');
        }
    }

    public function logout()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            header('Location:home');
        }
    }

    public function search($request)
    {

        if (empty($request['searchInput'])) {
            session_start();
            $_SESSION['homeERR'] = 'Must insert something in the search bar';
            header('Location:home');
        } else {
            $searchInput = $request['searchInput'];
            $userType = $request['userType'];

            $sql = "SELECT users.name as name,users.email as email, languages.name as `language` ,frameworks.name as framework, microframeworks.name as microframework 
            FROM users 
            LEFT JOIN roles ON roles.id = users.roles_id
            LEFT JOIN languages on languages.id = roles.language_id 
            LEFT join frameworks on frameworks.id = roles.framework_id 
            LEFT join microframeworks on microframeworks.id = roles.microframework_id
            WHERE roles.position_id = $userType AND (users.name LIKE '%$searchInput%' OR  users.email LIKE '%$searchInput%' )";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            session_start();

            $_SESSION['result'] = $result;
            header('Location:results');
        }
    }

    public function printTree()
    {

        $data = [];
        $sql1 = "   SELECT positions.name as position_name, languages.name as languages_name,
         frameworks.name  as frameworks_name, microframeworks.name as microframeworks_name,
          COUNT(users.id) as count FROM users 
        LEFT join roles on roles.id= users.roles_id
         LEFT JOIN languages on languages.id = roles.language_id 
                    LEFT join frameworks on frameworks.id = roles.framework_id 
                    LEFT join microframeworks on microframeworks.id = roles.microframework_id
                    LEFT JOIN positions on positions.id = roles.position_id
                    
                    GROUP by users.roles_id ";
        $stmt = $this->pdo->prepare($sql1);
        $stmt->execute();

        $queryData = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($queryData as $key => $data1) {

            if(!in_array($data1->position_name, $data) && $data1->position_name != null){
                    $key = $data1->position_name;
                    $data[$key] = (int)($data1->count);
            }
                    
            if(!in_array($data1->languages_name, $data) && $data1->languages_name != null){
                    $key = $data1->languages_name;
                    $data[$key] = (int)($data1->count);
            }
                    
            if(!in_array($data1->microframeworks_name, $data) && $data1->microframeworks_name != null) {
                    $key = $data1->microframeworks_name;
                    $data[$key] = (int)($data1->count);
            }
                            
            if (!in_array($data1->frameworks_name, $data) && $data1->frameworks_name != null) {
                    $key = $data1->frameworks_name;               
                    $data[$key] = (int)($data1->count);
            }    
        }
        // var_dump($data);
    }
}
